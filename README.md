# Docker-scc

An Alpine-based Docker image to run [scc](https://github.com/boyter/scc/) based on the [project recommendation](https://github.com/boyter/scc/#containers).
